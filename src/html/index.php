<?php  ?>
<html lang="en">
<head>
    <title>Copy Me</title>
    	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="build/css/styles.css">
</head>
<body>

<?php include ('section1.php'); ?>
<?php include ('section2.php'); ?>

<!-- Scripts -->
<script src="build/js/index.js"></script>
</body>
</html>
